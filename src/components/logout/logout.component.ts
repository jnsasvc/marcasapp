import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { HttpService } from 'src/services/brand.service';

@Component({
    selector: 'auth-logout',
    templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {
    constructor(private httpservice: HttpService) { }
    @Output() logoutResponse = new EventEmitter;

    ngOnInit(): void { }

    logout(){
        this.httpservice.setToken='';

        console.log(this.httpservice.setToken)
        this.logoutResponse.emit(false);
    }
    
}
