import { Component,OnInit,Output,EventEmitter, Input } from '@angular/core';
import { Brand } from '../../models/brand';
import { HttpService } from "../../services/brand.service";

@Component({
    selector: 'brand-list',
    templateUrl: './brandlist.component.html',
    providers:[HttpService]
})


export class BrandList {
    brands:Brand[];

    @Output() sendBrand = new EventEmitter;
    @Output() deleteBrand = new EventEmitter;


    constructor(private httpService:HttpService){

    }
    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
       this.httpService.getBrand().subscribe((data)=>this.brands=data);
        
    }
    upBrand($event){
        this.sendBrand.emit($event.brand);
    }
    
    @Input() set brand (value:Brand){
        if(this.brands)
        this.brands.push(value);
    }

    deletebrand($event){
        let res = this.httpService.deletebrand($event.brand);

        res.subscribe((data)=>{
            if(data ==1){
                this.brands.splice($event.i,1)
                this.deleteBrand.emit();
            }
        },
        (error)=>{
            console.log(error);
        }
        );
    }
}   