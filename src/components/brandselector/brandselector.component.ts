import { Component,OnInit,Output,EventEmitter, Input } from '@angular/core';
import { send } from 'process';
import { Brand } from '../../models/brand';
import { HttpService } from "../../services/brand.service";

@Component({
    selector: 'brand-selector',
    templateUrl: './brandselector.component.html',
    providers:[HttpService]
})


export class BrandSelector {
    brands:Brand[];
    selectedbrand:Brand;

    @Output() sendBrand = new EventEmitter;
    constructor(private httpService:HttpService){

    }
    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
       this.httpService.getBrand().subscribe((data)=>this.brands=data);
        
    }

    setBrand(val){
        this.selectedbrand= val;
        this.sendBrand.emit(val);
    }

}   
