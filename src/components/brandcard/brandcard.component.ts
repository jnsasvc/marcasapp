import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Brand } from '../../models/brand';
import { HttpService } from "../../services/brand.service";

@Component({
    selector: 'brand-card',
    templateUrl: './brandcard.component.html',
})

export class BrandCard implements OnInit{
    @Input() brand: Brand;
    @Input() index: number;

    @Output() sendBrand = new EventEmitter;
    @Output() deleteBrand = new EventEmitter;


    constructor(private httpService:HttpService){

    }

    ngOnInit(): void { }

    update(){
        this.sendBrand.emit({brand:this.brand,i:this.index});
    }

    destroy(){
        this.deleteBrand.emit({brand:this.brand,i:this.index})
    }
}
