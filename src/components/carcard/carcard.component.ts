import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Car } from '../../models/car';
import { HttpService } from "../../services/brand.service";

@Component({
    selector: 'car-card',
    templateUrl: './carcard.component.html',
})

export class CarCard implements OnInit{
    @Input() car: Car;
    @Input() index: number;

    @Output() sendCar = new EventEmitter;
    @Output() deleteCar = new EventEmitter;


    constructor(private httpService:HttpService){

    }

    ngOnInit(): void { }

    update(){
        this.sendCar.emit({car:this.car,i:this.index});
    }

    destroy(){
        this.deleteCar.emit({car:this.car,i:this.index})
    }
}
