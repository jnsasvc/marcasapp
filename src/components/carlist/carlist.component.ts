import { Component,OnInit,Output,EventEmitter, Input } from '@angular/core';
import { Brand } from 'src/models/brand';
import { Car } from '../../models/car';
import { HttpService } from "../../services/brand.service";

@Component({
    selector: 'car-list',
    templateUrl: './carlist.component.html',
    providers:[HttpService]
})


export class CarList {
    cars:Car[];

    @Output() sendCar = new EventEmitter;
    @Output() deleteCar = new EventEmitter;
    @Input() current: Brand;


    constructor(private httpService:HttpService){

    }
    ngOnInit(): void {
        if(this.current){
            this.httpService.getCarList(this.current.id).subscribe((data)=>this.cars=data);
        }
    }
    upCar($event){
        this.sendCar.emit($event.car);
    }
    
    @Input() set car (value:Car){
        if(this.cars)
        this.cars.push(value);
    }

    deletecar($event){
        let res = this.httpService.deletecar($event.car);

        res.subscribe((data)=>{
            if(data ==1){
                this.cars.splice($event.i,1)
                this.deleteCar.emit();
            }
        },
        (error)=>{
            console.log(error);
        }
        );
    }

    @Input() set currentBrand(value){
        if(value){
            this.current=value;
            this.httpService.getCarList(this.current.id).subscribe((data)=>this.cars=data);
        }
    }
}   