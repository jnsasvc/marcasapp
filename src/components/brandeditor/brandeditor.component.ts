import { Component, Input, OnInit,Output,EventEmitter } from '@angular/core';
import { Brand } from '../../models/brand';
import { HttpService } from "../../services/brand.service";

@Component({
  selector: 'brand-editor',
  templateUrl: './brandeditor.component.html',
})
export class BrandEditor implements OnInit {
  @Input() brand: Brand ;
  error= {
    name:'',
  };
  
  @Output() sendBrand = new EventEmitter;
  constructor(private httpservice:HttpService){

  }
  ngOnInit() {

  }

  cleannew(){
    this.brand ={
      name:'',
      id:0
    }
  }

  updatemodel(){
    let res =
    this.brand.id ==0?
    this.httpservice.storebrand(this.brand):
    this.httpservice.updatebrand(this.brand);

    res.subscribe(
      (data:Brand)=> {
        if(this.brand.id==0){
          this.sendBrand.emit(data);
          this.cleannew();
        }
      },
      (error)=> {
        if(error.status==422){
          this.error=error.error;
        }else{
          console.log(error);
        }
      }
    );
  }

  @Input() set clean(value){
    if(value){
      this.cleannew();
    }
  }

}