import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { error } from 'protractor';
import {    HttpService   }    from '../../services/brand.service'

@Component({
    selector: 'auth-login',
    templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
    constructor(private httpservice: HttpService) { }
    mail:string;
    pass:string;
    error:{
        email:string,
        pass:string,
        general:string
    };

    @Output() loginResponse = new EventEmitter;

    ngOnInit(): void {
        this.error={
            email:'',
            pass:'',
            general:''
        };

     }

    async login(){
        let res = await this.httpservice.login(this.mail,this.pass).then(
           data => data
       ).catch(err => err);
        
       if(res.token){
           this.httpservice.setToken= res.token ;
       }
       
        if(this.httpservice.getToken!=undefined){
            this.loginResponse.emit(true);
        }else{
            this.error.general='Unauthorized Data'
        }
    }

}
