import { Component, Input, OnInit,Output,EventEmitter } from '@angular/core';
import { Brand } from 'src/models/brand';
import { Car } from '../../models/car';
import { HttpService } from "../../services/brand.service";

@Component({
  selector: 'car-editor',
  templateUrl: './careditor.component.html',
})
export class CarEditor implements OnInit {
  @Input() car: Car ;
  @Input() brand:Brand;
  error= {
    name:'',
  };
  
  @Output() sendCar = new EventEmitter;
  constructor(private httpservice:HttpService){

  }
  ngOnInit() {

  }

  cleannew(){
    this.car ={
      name:'',
      id:0,
      brand_id:this.brand.id
    }
  }

  updatemodel(){
    let res =
    this.car.id ==0?
    this.httpservice.storecar(this.car):
    this.httpservice.updatecar(this.car);

    res.subscribe(
      (data:Car)=> {
        if(this.car.id==0){
          this.sendCar.emit(data);
          this.cleannew();
        }
      },
      (error)=> {
        if(error.status==422){
          this.error=error.error;
        }else{
          console.log(error);
        }
      }
    );
  }

  @Input() set clean(value){
    if(value){
      this.cleannew();
    }
  }

  @Input() set currentBrand(value){
    if(value){
      this.brand=value;
      this.car.brand_id=value.id;
    }
  }

}