import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';

import {BrandList} from '../components/brandlist/brandlist.component';
import {BrandCard} from '../components/brandcard/brandcard.component';
import {BrandEditor} from '../components/brandeditor/brandeditor.component';
import {CarList} from '../components/carlist/carlist.component';
import {CarCard} from '../components/carcard/carcard.component';
import {CarEditor} from '../components/careditor/careditor.component';
import {BrandSelector} from '../components/brandselector/brandselector.component';
import {HttpService} from '../services/brand.service';
import {LoginComponent } from '../components/login/login.component'
import { LogoutComponent} from '../components/logout/logout.component'






@NgModule({
  declarations: [
    AppComponent,
    BrandList,
    BrandCard,
    BrandEditor,
    CarList,
    CarCard,
    CarEditor,
    BrandSelector,
    LoginComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
