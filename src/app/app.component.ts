import { Component } from '@angular/core';
import { Brand } from 'src/models/brand';
import { Car } from 'src/models/car';
import { HttpService } from 'src/services/brand.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Marcas';
  brand: Brand=    {
    name:"",
    id:0
  };

  car: Car={
    name:"",
    id:0,
    brand_id:16
  };

  current:Brand;
  currentcar:Car;
  currentbrandforcars:Brand;
  clean:boolean;
  cleancar:boolean;
  todelete:number;
  authorized:Boolean;
  
  constructor(private httpservice:HttpService){}


  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    if(this.httpservice.getToken){
      this.authorized=true;
    }else{
      this.authorized=false;
    }
  }

  setBrand($event){
    this.brand= $event;
    this.clean=false;
  }

  setDown($event){
    this.current=$event;
    this.clean=false;

  }

  deleteBrand(){
    this.clean=true;
  }

  setCar($event){
    this.car= $event;
    this.cleancar=false;
  }

  setDownCar($event){
    this.currentcar=$event;
    this.cleancar=false;

  }

  deleteCar(){
    this.cleancar=true;
  }

  setBradforcars($event){
    this.currentbrandforcars = $event;
  }

  authorize(value:Boolean){
    this.authorized=value;
  }
}
