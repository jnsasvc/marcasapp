import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Brand } from "../models/brand";
import { Car } from 'src/models/car';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
    }),
    params:new HttpParams ( {
      fromObject:{
        'api_token': ''
      }
    })
}
@Injectable({
    providedIn: 'root'
  })
export class HttpService {

  constructor(private http: HttpClient) { 
    
    httpOptions.params = new HttpParams ( {
      fromObject:{
        'api_token':this.getToken
      }
    });
  }

  _token:string;
  getBrand():Observable<Brand[]>{
    return this.http.get<Brand[]>('/api/brands',httpOptions);
  }

  storebrand(brand: Brand){
    return this.http.post(
    'api/brands',
    brand,
    httpOptions
    )
  }

  updatebrand(brand:Brand){
    return this.http.put(
      `api/brands/${brand.id}`,
      brand,
      httpOptions
    )
  }

  deletebrand(brand:Brand){
    return this.http.delete(
      `api/brands/${brand.id}`,
      httpOptions
    )
  }

  getCarList(id):Observable<Car[]>{
    return this.http.get<Car[]>('/api/carlist/'+id,httpOptions);
  }

  storecar(car: Car){
    return this.http.post(
    'api/cars',
    car,
    httpOptions
    )
  }

  updatecar(car:Car){
    return this.http.put(
      `api/cars/${car.id}`,
      car,
      httpOptions
    )
  }

  deletecar(car:Car){
    return this.http.delete(
      `api/cars/${car.id}`,
      httpOptions
    )
  }

  async login(mail:string,pass:string) {

    return await this.http.get(`/api/login?email=${mail}&password=${pass}`,httpOptions).toPromise();
  }

  set setToken(value:string){
    localStorage.setItem('token', value);
    this._token=value;
    httpOptions.params = new HttpParams ( {
      fromObject:{
        'api_token':value
      }
    });
  }

  get getToken(){
    if(this._token){
      return this._token;
    }

    if(localStorage.getItem('token')){
      return localStorage.getItem('token');
    }else{
      return '';
    }
  }

}